<?php

namespace App;

use Config;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Zipper;

class Webservice extends Model {

	/**
	 *  This method is praivate method used to reuse the code in STSend
	 **/
	public function getSTSummarise($Year, $Quarter, $SubmissionId) {
		$Vsubmission = DB::table('VSubmission')
			->select('VSubmission.*')
			->where([
				['Year', $Year],
				['Quarter', $Quarter],
				['SubmissionId', $SubmissionId],
			])
			->get();

		return $Vsubmission;
	}

	/**
	 **   This method is Private method used to get pdf zip.
	 **/
	public function generatedZipForAttachment($quarter, $year, $additionalFilesToZip = array()) {
		//$folderPath = env("STATTACHMENT_PATH");
		$folderPath = Config::get('url.STATTACHMENT_PATH');
		Log::info("folderPath" . $folderPath);
		//$compareYr = $year + 543;
		$fileName = $quarter . $year;
		$files = glob($folderPath . DIRECTORY_SEPARATOR . "*" . $fileName . "*");
		$totalFiles = sizeof($files);
		$zipFilePath = public_path($fileName . ".zip");
		$zipper = \Zipper::make($zipFilePath);
		foreach ($files as $file) {
			$zipper->add($file);
		}

		if (sizeof($additionalFilesToZip) > 0) {
			foreach ($additionalFilesToZip as $filePath) {
				$zipper->add($filePath);
			}
		}
		$zipper->close();

		//unlink($zipFilePath);
		return array($fileName => $zipFilePath);
	}
	public function getSTAttachment($quarter, $year) {
		$filesArray = $this->generatedZipForAttachment($quarter, $year);
		foreach ($filesArray as $fileName => $zipFilePath) {
			return $zipFilePath;
		}
	}

	public function getSTAttachmentForSTReceive($quarter, $year, $letterFilePath) {
		$additionalFilesToZip = array();
		array_push($additionalFilesToZip, $letterFilePath);
		$filesArray = $this->generatedZipForAttachment($quarter, $year, $additionalFilesToZip);
		Log::info("stAttachment filesArray: " . json_encode($filesArray));
		$totalFileSize = 0;
		foreach ($filesArray as $key => $filePath) {
			$fileSize = round((filesize($filePath) / 1024) / 1024, 1); //This will return the size of zip file in MB
			Log::info("Size of the file " . $filePath . " is: " . $fileSize);
			$totalFileSize += $fileSize; //This will be the size of all the files in the filesArray
		}

		/*
			 * Compare the total filesize (The filesize of all the files in an array) to 1 GB i.e. 1024 MB
			 * If the total fileSize is greater than 1 GB i.e. 1024 MB then throw an exception else
			 * return the filesArray
		*/
		Log::info("Size of all files in the filesArray: " . $totalFileSize);
		if ($totalFileSize > 1024) {
			//throw new Exception("File size is greater than 1 GB");
			throw new Exception("ไม่สามารถนำส่งได้ เนื่องจากไฟล์ทั้งหมดที่เลือกมีขนาดรวมกันใหญ่เกินกว่า 1 กิกะไบต์");
		} else {
			return $filesArray;
		}
	}

	/**
	 ** This method is used to get the Receipt PDf file Path
	 **/
	public function getPetitionRecords($quarter, $year) {
		try {
			//$folderPath = public_path() . DIRECTORY_SEPARATOR . "PDF_Files";
			//$folderPath = env("STATTACHMENT_PATH");
			$folderPath = Config::get('url.STATTACHMENT_PATH');
			Log::info("ReceiptfilefolderPath" . $folderPath);
			//$compareYr = $year + 543;
			$fileName = $quarter . $year; ///12560
			Log::info("fileName: " . $fileName);
			$files = glob($folderPath . DIRECTORY_SEPARATOR . "*" . $fileName . "*");
			$mainArr = array();
			if (sizeof($files) > 0) {
				foreach ($files as $filepath) {
					//Log::info("filesArray==>" . json_encode($files));
					//$mainArr['filePath'] = $filepath;
					$filename = basename($filepath);
					$fNameArr = explode(".", $filename);
					//$fileNameNoExtension = preg_replace("/\.[^.]+$/", "", $fileName);
					Log::info("fNameArr[0]: " . $fNameArr[0]);

					$fNamePart1 = substr($fNameArr[0], 7);
					$yearPart = (int) substr($fNamePart1, 0, -5);
					//$yearDisplay = (int) substr($fNamePart1, 2, -5);
					$qtrYearString = $yearPart . "";

					$calYear = substr($qtrYearString, 1);
					$calQuarter = substr($qtrYearString, 0, 1);

					//$downloadEndpoint = "download/receipt/" . $fNameArr[0] . "/" . $fNameArr[1];
					$downloadEndpoint = "download/receipt/" . base64_url_encode($filename);

					array_push($mainArr, array('fileName' => $filename, 'Year' => $calYear, 'quarter' => $calQuarter, 'downloadEndpoint' => $downloadEndpoint));
				}
			} else {
				Log::info("No file found! files: " . json_encode($files));
			}

			return $mainArr;
		} catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);
		}
	}

	/**
	 ** This method is used to get Count of submission to submit the NO in streceive.
	 **/
	public function getNoOfSubmissions($finYear, $quarter) {
		Log::info("finYear==>" . $finYear);
		Log::info("quarter==>" . $quarter);
		$submissionCountQuery = DB::table('Submission')
			->where('Submission.Quarter', '=', $quarter)
			->where('Submission.Year', '=', $finYear);
		$submissionCount = $submissionCountQuery->get()->count();

		return $submissionCount;
	}

	public function getReceiptRecords($finYear, $quarter) {
		$arr = DB::table("VReceipt")
			->where('FiscalYear', '=', $finYear)
			->where('FiscalQuarter', '=', $quarter)
			->limit(1)
			->get();

		return $arr;
	}
}
