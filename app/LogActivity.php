<?php


namespace App;


use Illuminate\Database\Eloquent\Model;


class LogActivity extends Model
{
    protected $table = 'AccessLog';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Subject', 'Url', 'Method', 'IpAddress', 'Agent', 'User'
    ];
}