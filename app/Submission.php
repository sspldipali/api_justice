<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;

class Submission extends Model
{
	protected $table = 'Submission';
	/*const CREATED_AT = 'ChargeCreatedOn';
    const UPDATED_AT = 'DischargeApprovedOn';*/
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';
	protected $primaryKey = 'id';
	protected $fillable = [

		'id', 'Year', 'Quarter', 'User', 'No', 'LetterNo', 'LetterDate', 'LetterFilePath',
		'FileCount', 'SubmitDate', 'NoOfCases', 'CallbackDate', 'Error', 'created_at', 'updated_at',
	];

	public function saveNewSubmission($submission)
	{
		return $submission->save();
	}

	public function getVSubmissionData($year, $quarter)
	{
		$result = DB::table("Vsubmission AS vs")
			->where("vs.Year", "=", $year)
			->where("vs.Quarter", "=", $quarter)
			->get();
		return $result;
	}

	public function getFYear()
	{
		try {
			Log::info(json_encode(collect(DB::select('EXEC FiscalYear ?', array('2019-12-28')))));
			//$arr = DB::select('SELECT FiscalYear(?) AS finYear', ['2019-12-28']);
			//$queryResult = DB::select('EXEC FiscalYear "2019-12-28"');
			//$results = collect($queryResult);
			//Log::info("==> " . json_encode($arr));
			//return response()->json($arr);
		} catch (\Exception $e) {
			Log::error($e);
		}
	}

	public function getSubmissionReportPreview($finYear, $quarter)
	{
		$previewData = DB::table("VSubmissionPreview")
			->where("Year", $finYear)
			->where("Quarter", $quarter)
			->get();
		return $previewData;
	}

	public function getSubmissionReporHistory($finYear, $quarter)
	{
		$historyData = DB::table("Submission")
		    ->join("users AS u", "u.id", "=", "Submission.User")
			->where("Year", $finYear)
			->where("Quarter", $quarter)
			->select('u.*','Submission.*')
			->get();
	    //Log:info('historyDataModel'.$historyData);
		return $historyData;
	}
	
	/**
	**  This method is used to shoe the submission details
	**/
	public function getSubmissionDetailsBySubmissionId($submissionID) {
		$submissionData = DB::table("Submission")
			->where("id", $submissionID)
			->select('Submission.*')
			->get();
		return $submissionData[0];
		
	}
}
