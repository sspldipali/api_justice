<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;
use DB;
class User extends Authenticatable implements JWTSubject
//class User extends Model implements Auditable

{
 
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'Email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

//get user email id
    public function getUserByEmailId($EmailID) {
        $query = DB::table('users')
      ->where('email',$EmailID);
      $userDetails = $query->get();
      if (sizeof($userDetails) > 0) {
          return $userDetails[0];
      } else {
          return null;
      }
   }
   

   //update password field

   public function updatePassword($id, $newPassword) {
    $user = User::find($id);
    if ($user != null) {
        $user->update(array("password" => $newPassword));
        return $user;
    } else {
        return null;
    }
}
    public function sendPasswordResetNotification($token)
{
    $this->notify(new \App\Notifications\MailResetPasswordNotification($token));

}
	/*
      * This function is used to get Menus by role_id 
      */
      public function getMenu($roleId, $parentMenuId = 0, $menuType = 1) {
        $menuSQL = "SELECT mm.id AS menu_id, mm.menu_name, mm.menu_path, mm.menu_icon, mm.href, mm.intro 
                   FROM menu_master AS mm, role_menu_mapping AS rmm
                   WHERE mm.id = rmm.menu_id
                       AND mm.menu_type = ?
                       AND mm.parent_menu_id = ?
                       AND rmm.role_id = ?
                       AND rmm.mapping_status = ? ORDER BY mm.id ";
        $parameterArray = array($menuType, $parentMenuId, $roleId, 1);
        $menuArray = DB::select($menuSQL, $parameterArray);
        return $menuArray;
    }
	/**
	** This method is used to get all teh userd
	*/
	
	 public function getAllUsersForNotification() {
		 
		  $users = DB::table('users')
                        ->join('Role', 'users.Role', '=', 'Role.Id')
                        ->select('users.*', 'Role.RoleName')
                        ->get();
		  return $users;
		  
	 }
}
