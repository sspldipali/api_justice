<?php
 
namespace App\Exceptions;
 
use Exception;
use Auth;
use App\DbLog;
use App\User;
use Config;
use Illuminate\Support\Facades\Session;

class LogData extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report(){
    }
 
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
      public function render($request, Exception $exception){
      $log = new DbLog;
      $log->User = auth()->check() ? auth()->user()->id : 9;
      $log->Action = $request->fullUrl();
      $log->Exception =$exception;
      $log->save(); 
      return \Redirect::back()->with('error', 'Something Went Wrong.');
    }
}