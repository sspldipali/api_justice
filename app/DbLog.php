<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use DB;
class DbLog extends Model
//class User extends Model implements Auditable

{
    protected $table = 'ErrorLog';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'User', 'Action', 'Exception',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    

}