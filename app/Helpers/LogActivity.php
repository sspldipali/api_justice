<?php


namespace App\Helpers;
use Request;
use App\LogActivity as LogActivityModel;


class LogActivity
{


    public static function addToLog($subject)
    {
    	$log = [];
    	$log['Subject'] = $subject;
    	$log['Url'] = Request::fullUrl();
    	$log['Method'] = Request::method();
    	$log['IpAddress'] = Request::ip();
    	$log['Agent'] = auth()->check() ? auth()->user()->Name : "Admin";
    	$log['User'] = auth()->check() ? auth()->user()->id : 1;
    	LogActivityModel::create($log);
    }


    public static function logActivityLists()
    {
    	return LogActivityModel::latest()->get();
    }


}