<?php

/**
 * @author Dipali Patil
 */

if (!function_exists("base64_url_encode")) {
	function base64_url_encode($data) {
		return strtr(base64_encode($data), '+/=', '-_,');
	}
}

if (!function_exists("base64_url_decode")) {
	function base64_url_decode($data) {
		return base64_decode(strtr($data, '-_,', '+/='));
	}
}

if (!function_exists("logInfo")) {
	function logInfo($logMessage) {
		Log::info($logMessage);
	}
}

if (!function_exists("logObject")) {
	function logObject($object, $logText = "") {
		Log::info($logText . json_encode($object));
	}
}

if (!function_exists("logException")) {
	function logException($exception, $exceptionMessage = "") {
		Log::info($exceptionMessage . ": " . $exception->getMessage());
		Log::error($exception);
	}
}