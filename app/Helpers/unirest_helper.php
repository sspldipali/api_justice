<?php

/**
 * @author Dipali Patil
 */

if (!function_exists("postWithJsonBody")) {
	function postWithJsonBody($endpoint, $postBody, $headers = array(
		'Accept' => 'application/json',
		'Content-Type' => 'application/json',
	)) {
		Log::info("Creating unirest request body...");
		$body = \Unirest\Request\Body::json($postBody);
		Log::info("Unirest request body created!");
		Log::info("Calling POST API: " . env("STRECEVE_URL") . $endpoint);
		Unirest\Request::timeout(9000); //900 sec = 15 minutes
		$response = \Unirest\Request::post(env("STRECEVE_URL") . $endpoint, $headers, $body);
		return $response;
	}
}
if (!function_exists("putWithJsonBody")) {
	function putWithJsonBody($endpoint, $putBody, $headers = array(
		'Accept' => 'application/json',
		'Content-Type' => 'application/json',
	)) {
		Log::info("Creating unirest request body...");
		$body = \Unirest\Request\Body::json($putBody);
		Log::info("Unirest request body created!");
		Log::info("Calling POST API: " . env("STRECEVE_URL") . $endpoint);
		Unirest\Request::timeout(9000); //900 sec = 15 minutes
		$response = \Unirest\Request::put(env("STRECEVE_URL") . $endpoint, $headers, $body);
		return $response;
	}
}

if (!function_exists("callGetAPI")) {
	function callGetAPI($endpoint, $parameters = array(), $headers = array(
		'Accept' => 'application/json',
		'Content-Type' => 'application/json')) {
		Log::info("Calling GET API: " . env("STRECEVE_URL") . $endpoint);
		$response = \Unirest\Request::get(env("STRECEVE_URL") . $endpoint, $headers, $parameters);
		return $response;
	}
}

if (!function_exists("postWithAttachments")) {
	function postWithAttachments($endpoint, $postBody, $filesArray, $headers = array(
		'Accept' => 'application/json',
		'Content-Type' => 'multipart/form-data',
	)) {
		try {
			Log::info("Creating unirest request body...");
			//Log::info("filesArray: " . json_encode($filesArray));
			$body = \Unirest\Request\Body::multipart($postBody, $filesArray);
			Log::info(json_encode($body));
			Log::info("Unirest request body created!");
			Log::info("Calling POST API: " . env("STRECEVE_URL") . $endpoint);
			Log::info("headers: " . json_encode($headers));
			$response = \Unirest\Request::post(env("STRECEVE_URL") . $endpoint, $headers, $body);
			return $response;
		} catch (\Exception $e) {
			Log::error($e);
			throw $e;
		}
	}
}
