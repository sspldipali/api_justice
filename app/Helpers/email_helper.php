<?php

use PHPMailer\PHPMailer\PHPMailer;
//use Config;
if (!function_exists("sendEmail")) {
    function sendEmail($to, $subject, $emailBody, $cc = "", $bcc = "")
    {
		   $email = Config::get('url.username');
		   $password = Config::get('url.password');
        
            $mail = new PHPMailer();
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = "utf-8"; // set charset to utf8
            $mail->SMTPAuth = true;  // use smpt auth
            //$mail->SMTPSecure = "ssl"; // or ssl
            //$mail->Host = "smtp.gmail.com";
            $mail->Host = 'outgoing.mail.go.th:25';
            $mail->Port = 25; // most likely something different for you. This is the mailtrap.io port i use for testing.
            $mail->Username = $email;
			
            $mail->Password = $password;
            $mail->setFrom($email,"สำนักงานศาลยุติธรรม");
            //$mail->SMTPDebug  = 3;
            $mail->SMTPAutoTLS = false;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->Subject = $subject;
            $mail->MsgHTML($emailBody);
            $mail->AddAddress($to);

            if ($cc != "") {
                $mail->AddCC($cc);
            }

            if ($bcc != "") {
                $mail->AddBCC($bcc);
            }

            Log::info("==============================");
            Log::info("Sending email to: ");
            Log::info("TO: " . $to);
            Log::info("CC: " . $cc);
            Log::info("BCC: " . $bcc);
            Log::info("==============================");

            //$mail->SMTPDebug  = 3;
            $isSent = $mail->send();
            Log::info("Email sent flag: " . $isSent);
            return $isSent;
       
    }
}
