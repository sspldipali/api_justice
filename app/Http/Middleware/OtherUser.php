<?php

namespace App\Http\Middleware;

/**
 * @author Dipali Patil
 */
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;
use Closure;

class OtherUser {

    public function handle($request, Closure $next) {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if ($user) {
                if ($user->Role == 3 || $user->Role == 2 || $user->Role == 1) {
                    return $next($request);
                } else {
                    return response()->json(['message' => 'You are not authorized to access this Web API'], 401);
                }
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['message' => 'Token is Invalid']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['message' => 'Token is Expired']);
            }else{
                return response()->json(['message' => 'Authorization Token not found']);
            }
        }
    }
}