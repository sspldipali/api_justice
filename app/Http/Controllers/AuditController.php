<?php

namespace App\Http\Controllers;

use App\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AuditController extends Controller
{

    /*
     *This method used to delete error log record by id in access log screen
     */
    public function delete_access_report($id)
    {
        $affected = DB::table('AccessLog')
            ->where('Id', $id)
            ->delete();
        if ($affected) {
            return response(array(
                'error' => false,
                'message' => 'log_activities deleted successfully',
                'user' => $affected,

            ), 200);
        } else {
            return response(array(
                'error' => true,
                'message' => 'audits id not found',
                'user' => $affected,

            ), 404);
        }

    }

    /*
     *This method used to get error log data in error log screen
     */
    public function getErrorLog(Request $request)
    {
        try {
            if ($input = $request->only('from_date', 'to_date')) {
                $start = date("Y-m-d", strtotime($request->input('from_date')));
                $end = date("Y-m-d", strtotime($request->input('to_date') . "+1 day"));
                $users = DB::table('ErrorLog As l')
                    ->join('users', 'users.id', '=', 'l.User')
                    ->select('l.*', 'users.Name')
                    ->whereBetween('l.created_at', [$start, $end])
                    ->get();
                return response()->json(['user' => $users]);

            }

        } catch (\Exception $e) {
            Log::info("Excession Occured".$e);
        }

    }

/*
 *This method used to delete error log data by id  in error log screen
 */
    public function delete_error_log($id)
    {
        try {
            $affected = DB::table('ErrorLog')
                ->where('Id', $id)
                ->delete();
            if ($affected) {
                $users = DB::table('ErrorLog')

                    ->select('ErrorLog.*')
                    ->get();
                return response(array(
                    'error' => false,
                    'message' => 'db_logs deleted successfully',
                    'user' => $users,

                ), 200);
            } else {
                return response(array(
                    'error' => true,
                    'message' => 'db_logs id not found',
                    'user' => $affected,

                ), 404);
            }

        } catch (\Exception $e) {
            Log::info("Excession Occured==>".$e);
        }

    }
/*
 *This method used to get audit log data for log activity in access log screen
 */
    public function get_access_log_data()
    {
        try {
            $users = DB::table('AccessLog')
                ->select('AccessLog.*')
                ->get();
            return response()->json(['user' => $users]);
        } catch (\Exception $e) {
            Log::info("Excession Occured".$e);
            Log::info($e);
        }
    }

    public function myTestAddToLog()
    {
        \LogActivity::addToLog('My Testing Add To Log.');
        dd('log insert successfully.');
    }

/**
 * Show the application dashboard.
 *
 * @return \Illuminate\Http\Response
 */
    public function logActivity()
    {
        $logs = \LogActivity::logActivityLists();
        return view('logActivity', compact('logs'));
    }
}
