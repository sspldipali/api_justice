<?php

namespace App\Http\Controllers;

use App\Submission;
use App\User;
use App\Webservice;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use JWTAuth;
use Unirest\Request\Body;

class SubmissionController extends Controller {

	private $controllerModel = null;

	public function __construct() {
		$this->controllerModel = new Submission();
	}

	public function checkSTReceive(Request $request) {
		try {
			$response = callGetAPI("/api/service/check");
			if ($response->code == 200) {
				return response()->json(array("message" => "STReceive service is working..."), 200);
			} else {
				return response()->json(array("message" => "Unable to check STRecieve service"), 400);
			}
		} catch (\Exception $e) {
			Log::info("Exception while checking STReceive service");
			Log::error($e);
			return response()->json(array("message" => "Unable to connect to STReceive"), 400);
		}
	}
	/**
	 * This function is used to submit ST Send data
	 */
	public function saveSubmission(Request $request) {
		logInfo("Data received at: saveSubmission");
		$data = array();
		try {
			$submissionModel = new Submission();
			$requestBody = $request->all();

			Log::info("requestBody: " . json_encode($requestBody));
			//Log::info("requestBody: " . json_encode($requestBody));
			//Log::info("param: " . $requestBody['finYear']);

			$submissionModel->Year = $requestBody['finYear'];
			$submissionModel->Quarter = $requestBody['quarter'];
			$submissionModel->No = $requestBody['noOfSubmission'];
			$submissionModel->NoOfCases = $requestBody['noOfCases'];
			//Log::info("JWTAuth::user(): " . json_encode(JWTAuth::user()));
			logObject(JWTAuth::user(), "JWTAuth::user(): ");
			$submissionModel->User = JWTAuth::user()->id;
			$submissionModel->LetterNo = $requestBody['letterNo'];
			if ($requestBody['letterDate'] != "" && $requestBody['letterDate'] != null) {
				$submissionModel->LetterDate = $requestBody['letterDate'];
			} else {
				$submissionModel->LetterDate = null;
			}
			foreach ($request->all() as $key => $value) {
				if ($request->hasFile($key)) {
					$file = $request->file($key);
					$submissionModel->LetterFilePath = $file;
				}
			}
			$submissionModel->LetterFilePath = $requestBody['letterFilePath'];
			$submissionModel->FileCount = $requestBody['fileCount'];
			//$submissionModel->SubmitDate = $requestBody['SubmitDate'];
			$submissionModel->SubmitDate = Carbon::now()->toDateString();

			$isSaved = $submissionModel->save();
			$isSaved = true;
			if ($isSaved) {
				$webserviceModel = new Webservice();
				$vSubmissionData = $webserviceModel->getSTSummarise($requestBody['finYear'], $requestBody['quarter'], $submissionModel->id);
				//Log::info("Summarise Data==>" . json_encode($vSubmissionData));
				//logObject($vSubmissionData, "Summatise Data===>");

				//$stAttachment = $webserviceModel->getSTAttachment($requestBody['Quarter'], $requestBody['Year']);
				$stAttachment = array();
				try {
					$stAttachment = $webserviceModel->getSTAttachmentForSTReceive($requestBody['quarter'], $requestBody['finYear'], $submissionModel->LetterFilePath);
				} catch (\Exception $e) {
					logException($e, "Exception: ");
					/*Log::info("Exception: " . $e->getMessage());
					Log::error($e);*/
					return response(array("message" => $e->getMessage()), 400, array("Content-Type" => "application/json"));
				}

				//Post data with attachments
				//Log::info("stAttachment: " . json_encode($stAttachment));
				//logObject($stAttachment, "stAttachment: ");

				$data = array();
				$vSubmissionObj = null;

				//To submit multiple records to streceive
				$mainDataArr = array();
				$submissionRecords = array();
				$dArrIdx = 0;
				foreach ($vSubmissionData as $vSubmissionObj) {
					$submissionData = array();
					foreach ($vSubmissionObj as $key => $value) {
						$submissionData[$key] = $value;
					}
					$submissionData['NoOfCases'] = $requestBody['noOfCases'];
					$submissionData['SubmitDate'] = Carbon::now()->toDateString();
					$submissionData['FileCount'] = $requestBody['fileCount'];
					$submissionData['No'] = $requestBody['noOfSubmission'];
					array_push($submissionRecords, json_decode(json_encode($submissionData)));
					//$mainDataArr[$dArrIdx] = json_encode($submissionData);
					$dArrIdx = $dArrIdx + 1;
				}
				$mainDataArr['submissionRecords'] = json_encode($submissionRecords);
				//comment to submit multiple records to streceive
				/*if (sizeof($vSubmissionData) > 0) {
					$vSubmissionObj = $vSubmissionData[0];
				}

				foreach ($vSubmissionObj as $key => $value) {
					$data[$key] = $value;
				}

				$data['NoOfCases'] = $requestBody['noOfCases'];
				$data['SubmitDate'] = Carbon::now()->toDateString();
				$data['FileCount'] = $requestBody['fileCount'];
				$data['No'] = $requestBody['noOfSubmission'];
				*/
				//comment ends

				//Get Receipt List
				$receiptObjList = $webserviceModel->getReceiptRecords($requestBody['finYear'], $requestBody['quarter']);

				$receiptList = array();

				foreach ($receiptObjList as $receiptObj) {
					/*$receipt = array();
						foreach ($receiptObj as $key => $value) {
							$receipt[$key] = $value;
					*/
					array_push($receiptList, json_decode(json_encode($receiptObj)));
				}

				$stReceiveSuccess = false;
				$response = array();
				//Log::info("maindataArr==>" . json_encode($mainDataArr));
				$mainDataArr['receiptList'] = json_encode($receiptList);
				try {
					$response = postWithAttachments("/api/STReceive", $mainDataArr, $stAttachment);
					//Log::info("responseStreceive: " . json_encode($response));
					logObject($response, "responseSTreceive: ");
					$stReceiveSuccess = true;
				} catch (\Exception $ex) {
					Log::info("Something went wrong while sending data in STReceive. Exception: " . $ex->getMessage());
					Log::error($ex);
					$stReceiveSuccess = false;
				}
				$respNode = array();
				if ($stReceiveSuccess == true) {
					$respNode['data'] = $response->body;
					$respNode['stReceiveSuccess'] = true;
				} else {
					$respNode['data'] = array();
					$respNode['stReceiveSuccess'] = false;
				}

				$respNode['message'] = "บันทึกรายการนำส่งสำเร็จ!";
				//Delete the stAttahcment temp zip file
				foreach ($stAttachment as $key => $zipFilePath) {
					unlink($zipFilePath);
				}

				//Send notification email for submission to logged in user
				pclose(popen("start /B curl " . url('/') . "/api/send/email/submission/" . $requestBody['finYear'] . "/" . $requestBody['quarter'], "r"));
				/*$userModel = new User();
					$allUsers= $userModel->getAllUsersForNotification();
					Log::info("all Users===>".json_encode($allUsers));
					foreach($allUsers as $userObj) {
						$userEmail = $userObj->Email;
						Log::info("EMails".$userEmail);
						$notification = $userObj->Notification;
						if($notification ==1) {
							$url = env("Notification_URL");
							$userEmail = $userObj->Email;
							$subject = "รายงานข้อมูลการสืบหาหลักทรัพย์และการผ่อนชำระค่าปรับกรณีที่ผิดสัญญาประกันในคดีอาญา ประจำปีที่    ".  $data['Year'] ."   ไตรมาสที่    " .$data['Quarter'];
							$emailBody = $finalBody ="เรียน คุณ  <b>" .$userObj->Name. "</b><br>".
							"<p>ท่านสามารถเข้าดูรายงานข้อมูลการสืบหาหลักทรัพย์และการผ่อนชำระค่าปรับกรณีที่ผิดสัญญาประกันในคดีอาญา ประจำปีที่   <b>" .$data['Year']. " </b> ไตรมาสที่  <b>" .$data['Quarter']. " </b> ได้ที่  " .$url. "<br>".
							"<p style='text-align:right;'><b>สำนักงานศาลยุติธรรม<b></p><br>";

							sendEmail($userEmail, $subject, $emailBody);
						}

				*/
				/*$loggedInUser = JWTAuth::user();
					if ($loggedInUser!= null) {
						if ($loggedInUser->Notification == 1) {
							$loggedInUserEmail = $loggedInUser->Email;
							$subject = "Submission Notification";
							$emailBody = "Email Body";
							sendEmail($loggedInUserEmail, $subject, $emailBody);
						}
				*/

				return response($respNode, 200, array("Content-Type" => "application/json"));
			} else {
				return response(
					array('message' => 'มีบางอย่างผิดพลาด!', 'success' => false),
					400,
					array("Content-Type" => "application/json")
				);
			}
		} catch (Exception $e) {
			Log::info("Exception occured while save submission details! Exception Message: " . $e->getMessage());
			$data['exception_message'] = "มีบางอย่างผิดพลาด...";
			Log::error($e);
			return response($data, 400, array("Content-Type" => "application/json", "msg" => $e->getMessage()));
		}
	}

	public function stReceiveService(Request $request) {
		$input = $request->all();
		Log::info(json_encode($input));
		return response($input, 200, array("Content-Type" => "application/json"));
	}

	/**
	 * This method is used to getReportPreviewdata
	 **/
	public function getReportPreview(Request $request) {
		try {
			Log::info("Reached SubmissionController@getReportPreview()");
			Log::info("Request Body: " . json_encode($request->all()));
			$reqData = $request->all();
			$previewData = $this->controllerModel->getSubmissionReportPreview($reqData['finYear'], $reqData['quarter']);

			$mainFileNameArr = array();
			$webserviceModel = new Webservice();
			$petitionList = $webserviceModel->getPetitionRecords($reqData['quarter'], $reqData['finYear']);
			$noOfSumission = $webserviceModel->getNoOfSubmissions($reqData['finYear'], $reqData['quarter']);
			Log::info("noOfSumission===>" . json_encode($noOfSumission));
			$noOfSumissionCountNO = $noOfSumission + 1;

			$receiptList = $webserviceModel->getReceiptRecords($reqData['finYear'], $reqData['quarter']);
			return response()->json(array(
				"success" => true,
				"message" => "Preview data returned successfully!",
				//"data" => $previewData
				"data" => array(
					'caseList' => $previewData,
					'petitionList' => $petitionList,
					'receiptList' => $receiptList,
					'noOfSubmission' => $noOfSumissionCountNO,
				),
			), 200);
		} catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(array(
				"success" => false,
				"message" => "Something went wrong while fetching report preview data!",
				"exception" => $e->getMessage(),
			), 400);
		}
	}

	public function getFiscalYearQtr() {
		try {
			$queryResult = DB::select('EXEC LookupFiscalYear');
			$results = collect($queryResult);
			//Log::info("results".json_encode($results));
			$mainArr = array();
			/*foreach ($results as $years) {
				$yearArr = array();
				$qry = "select * from FiscalYr(getdate())";
				$yrQtr = DB::select($qry);
				//Log::info("yrQtr".json_encode($yrQtr));
				//$yearArr['year'] = $yrQtr[0]->FiscalYear;
				$yearArr['qtr'] = $yrQtr[0]->FiscalQtr;
				array_push($mainArr, $yearArr);
			}*/
			return response()->json(['Year' => $mainArr, "yrArr" => $results]);
		} catch (\Exception $e) {
			Log::info("Excession Occured=>" . $e->getMessage());
			Log::error($e);
			return response()->json(['message' => "Something went wrong!", "exception" => $e->getMessage()], 400);
		}
	}

	public function getReportHistory(Request $request) {
		try {
			Log::info("Reached SubmissionController@getReportHistory()");
			Log::info("Request Body: " . json_encode($request->all()));
			$reqData = $request->all();
			$historyData = $this->controllerModel->getSubmissionReporHistory($reqData['finYear'], $reqData['quarter']);
			//Log:info('historyData'.$historyData);
			return response()->json(array(
				"success" => true,
				"message" => "History data returned successfully!",
				"data" => $historyData,
			), 200);
		} catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(array(
				"success" => false,
				"message" => "Something went wrong while fetching report history data!",
				"exception" => $e->getMessage(),
			), 400);
		}
	}

	/*
		//POC for sending emails in background
		public function testEmail() {
			$toArr = array("gauravpatil32@gmail.com", "dpatil1390@gmail.com",
				"patildipa1390@gmail.com", "devphp11@gmail.com", "businesszoomer@gmail.com");
			$previewData = $this->controllerModel->testQuery();
			Log::info("previewData: " . json_encode($previewData));
			foreach ($toArr as $to) {
				$subject = "Test Email";
				$emailBody = "<p>Hello!</p>
					<p>This email is sent using PHPmailier and php cli</p>
					<p>Sveltoz</p>";
				$isSent = sendEmail($to, $subject, $emailBody);
			}
			return response()->json(array("message" => "Executed successfully!",
					"previewData" => $previewData, "isSent" => $isSent), 200);
		}

		public function testCLI() {
			//exec("curl http://api_justice.win/api/test/cli &");
			pclose(popen("start /B curl http://api_justice.win/api/test/cli", "r"));
			Log::info("Cron added");
			return response()->json(array("message" => "Executed successfully!"), 200);
	*/

	/**This method is used to send the notification email  for submission*/
	public function sendBackgroundEmailsForSubmissionNotification(Request $request, $finYear, $quarter) {
		try {
			$userModel = new User();
			$allUsers = $userModel->getAllUsersForNotification();
			Log::info("all Users===>" . json_encode($allUsers));
			foreach ($allUsers as $userObj) {
				$userEmail = $userObj->Email;
				Log::info("EMails" . $userEmail);
				$notification = $userObj->Notification;
				if ($notification == 1) {
					$url = env("Notification_URL");
					$userEmail = $userObj->Email;
					$subject = "รายงานข้อมูลการสืบหาหลักทรัพย์และการผ่อนชำระค่าปรับกรณีที่ผิดสัญญาประกันในคดีอาญา ประจำปีที่    " . $finYear . "   ไตรมาสที่    " . $quarter;

					$emailBody = $finalBody = "<p>เรียน คุณ " . $userObj->Name . "</p><br>" .
						"<p>ท่านสามารถเข้าดูรายงานแผนภูมิข้อมูลการสืบหาหลักทรัพย์และการผ่อนชำระค่าปรับกรณีที่ผิดสัญญาประกันในคดีอาญา ประจำปีที่ " . $finYear . " ไตรมาสที่ " . $quarter . " ได้ที่</p><br>" .
						"<p><a href=\"" . $url . "\" target=\"_blank\">" . $url . "</a></p><br>" .
						"<p>สำนักงานศาลยุติธรรม</p>";

					sendEmail($userEmail, $subject, $emailBody);
				}
			}
			return response()->json(array(
				"message" => "Email sent successfully!",
			), 200
			);
		} catch (\Exception $e) {
			Log::info("Exception while sending background email notification! Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(array(
				"message" => "Something went wrong while sending emails in background for submission",
				"exception" => $e->getMessage(),
			), 400
			);
		}
	}

	/* This method is used to get the submission data by ID
		**
	*/
	public function getSubmissionDetailsByID(Request $request, $submissionID) {
		try {
			$submissionModel = new Submission();
			$submissionRecord = $submissionModel->getSubmissionDetailsBySubmissionId($submissionID);
			$finYear = $submissionRecord->Year;
			$quarter = $submissionRecord->Quarter;

			$caseListData = $this->controllerModel->getSubmissionReportPreview($finYear, $quarter);

			$webserviceModel = new Webservice();
			$petitionList = $webserviceModel->getPetitionRecords($quarter, $finYear);

			$responseArray = array();

			$responseArray['caseList'] = $caseListData;
			$responseArray['petitionList'] = $petitionList;

			$responseArray['finYear'] = $finYear;
			$responseArray['quarter'] = $quarter;
			$responseArray['submissionRecord'] = $submissionRecord;

			Log::info("submissionRecord: " . json_encode($submissionRecord));

			$responseArray['message'] = "Submission data returned successfully";
			return response()->json($responseArray, 200);

		} catch (\Exception $e) {
			Log::info("Exception while getting Submission Record! Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(array(
				"message" => "Something went wrong while get submission record",
				"exception" => $e->getMessage(),
			), 400
			);
		}
		//where is the back button?
	}
	/* This method is used to back on delivery history page
		**
	*/
	public function getDeliveryPreviewForViewDetails(Request $request, $finYear, $quarter) {
		try {

			return response()->json($responseArray, 200);

		} catch (\Exception $e) {
			Log::info("Exception while getting Submission Record! Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(array(
				"message" => "Something went wrong while get submission record",
				"exception" => $e->getMessage(),
			), 400
			);
		}

	}
}
