<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
class ChartController extends Controller
{
    /**
     * This method used to get chart1_1 data for pie in
     */
    public function chart1()
    {
        try {
            $queryResult = DB::select('EXEC Chart1_1');
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_2 data for pie in
     */
    public function chart1_2()
    {
        try {
            $queryResult = DB::select('EXEC Chart1_2');
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    /**
     * This method used to get chart1_3 data for pie in
     */
    public function chart1_3()
    {
        try {
            $queryResult = DB::select('EXEC Chart1_3');
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    /**
     * This method used to get chart1_3 data for pie in
     */
    public function chart1_4()
    {
        try {
            $queryResult = DB::select('EXEC Chart1_4');
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_1 data for pie wi
     */

    public function chart2_1($Juridiction)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_1 :Juridiction"), [
                ':Juridiction' => $Juridiction
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_2 data for pie wi
     */
    public function chart2_2($Juridiction)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_2 :Juridiction"), [
                ':Juridiction' => $Juridiction
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    /**
     * This method used to get chart2_3 data for pie wi
     */
    public function chart2_3($Juridiction)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_3 :Juridiction"), [
                ':Juridiction' => $Juridiction
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    /**
     * This method used to get chart2_4 data for pie wi
     */
    public function chart2_4($Juridiction)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_4 :Juridiction"), [
                ':Juridiction' => $Juridiction
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    /**
     * This method used to get chart1_5 data for line in
     */
    public function chart1_5($FiscalYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_5 :FiscalYear"), [
                ':FiscalYear' => $FiscalYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_6 data for line in
     */
    public function chart1_6($FiscalYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_6 :FiscalYear"), [
                ':FiscalYear' => $FiscalYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_7 data for line in
     */
    public function chart1_7($FiscalYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_7 :FiscalYear"), [
                ':FiscalYear' => $FiscalYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_8 data for line in
     */
    public function chart1_8($FiscalYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_8 :FiscalYear"), [
                ':FiscalYear' => $FiscalYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_5 data for line within court
     */
    public function chart2_5($Jurisdiction, $FiscalYear)
    {
        try {
			try{
				$queryResult = DB::select(DB::raw("EXEC Chart2_5 :Jurisdiction, :FiscalYear"), [
					':Jurisdiction' => $Jurisdiction,
					':FiscalYear' => $FiscalYear
				]);
				$results = collect($queryResult);
				return response()->json($results);
			} catch (\PDOException $pE) {
                Log::info("Excession Occured" . $pE->getMessage());
                Log::error($pE);
                return response()->json(array("message" => $pE->getMessage()), 400);
            }
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_6 data for line within court
     */
    public function chart2_6($Jurisdiction, $FiscalYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_6 :Jurisdiction, :FiscalYear"), [
                ':Jurisdiction' => $Jurisdiction,
                ':FiscalYear' => $FiscalYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_7 data for line within court
     */
    public function chart2_7($Jurisdiction, $FiscalYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_7 :Jurisdiction, :FiscalYear"), [
                ':Jurisdiction' => $Jurisdiction,
                ':FiscalYear' => $FiscalYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_8 data for line within court
     */
    public function chart2_8($Jurisdiction, $FiscalYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_8 :Jurisdiction, :FiscalYear"), [
                ':Jurisdiction' => $Jurisdiction,
                ':FiscalYear' => $FiscalYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_9 data for  bar charts including courts
     */

    public function chart1_9($PrecedingQtr)
    {
        try {
            try {
                $queryResult = DB::select(DB::raw("EXEC Chart1_9 :PrecedingQtr"), [
                    ':PrecedingQtr' => $PrecedingQtr
                ]);
                $results = collect($queryResult);
                return response()->json($results, 200);
            } catch (\PDOException $pE) {
                Log::info("Excession Occured" . $pE->getMessage());
                Log::error($pE);
                return response()->json(array("message" => $pE->getMessage()), 400);
            }
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
            Log::error($e);
            return response()->json(array("message" => $e->getMessage()), 400);
        }
    }
    /**
     * This method used to get chart1_10 data for  bar charts including courts
     */

    public function chart1_10($PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_10 :PrecedingQtr"), [
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_11 data for  bar charts including courts
     */

    public function chart1_11($PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_11 :PrecedingQtr"), [
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_12 data for  bar charts including courts
     */

    public function chart1_12($PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_12 :PrecedingQtr"), [
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_13 data for  bar charts including courts
     */

    public function chart1_13($PrecedingYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_13 :PrecedingYear"), [
                ':PrecedingYear' => $PrecedingYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_14 data for  bar charts including courts
     */

    public function chart1_14($PrecedingYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_14 :PrecedingYear"), [
                ':PrecedingYear' => $PrecedingYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_15 data for  bar charts including courts
     */

    public function chart1_15($PrecedingYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_15 :PrecedingYear"), [
                ':PrecedingYear' => $PrecedingYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart1_16 data for  bar charts including courts
     */

    public function chart1_16($PrecedingYear)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart1_16 :PrecedingYear"), [
                ':PrecedingYear' => $PrecedingYear
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    /**
     * This method used to get chart2_9 data for  bar charts within courts
     */

    public function chart2_9($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_9 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_10 data for  bar charts within courts
     */
    public function chart2_10($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_10 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_11 data for  bar charts within courts
     */
    public function chart2_11($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_11 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_12 data for  bar charts within courts
     */
    public function chart2_12($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_12 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_13 data for  bar charts within courts
     */
    public function chart2_13($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_13 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_14 data for  bar charts within courts
     */
    public function chart2_14($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_14 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_15 data for  bar charts within courts
     */
    public function chart2_15($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_15 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    /**
     * This method used to get chart2_16 data for  bar charts within courts
     */
    public function chart2_16($Jurisdiction, $PrecedingQtr)
    {
        try {
            $queryResult = DB::select(DB::raw("EXEC Chart2_16 :Jurisdiction, :PrecedingQtr"), [
                ':Jurisdiction' => $Jurisdiction,
                ':PrecedingQtr' => $PrecedingQtr
            ]);
            $results = collect($queryResult);
            return response()->json($results);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    //get juridiction values

    public function FiscalYear()
    {
        try {
            $queryResult = DB::select('EXEC LookupFiscalYear');
            $results = collect($queryResult);
            return response()->json(['user' => $results]);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }

    public function Juristiction(Request $request)
    { 
		$Court = $request->Court;	
		Log::info("Court===>".$Court);
        try {
            $queryResult = DB::select(DB::raw("EXEC LookupJurisdiction :Court"), [
				':Court' => $Court
			]);
            $results = collect($queryResult);
			Log::info("CourtLookResult==>".json_encode($results));
            return response()->json(['user' => $results]);
        } catch (\Exception $e) {
			Log::error($e);
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    //lookup PrecedingQtr

    public function PrecedingQtr()
    {
        try {
            $queryResult = DB::select('EXEC LookupPrecedingQtr');
            $results = collect($queryResult);
            return response()->json(['user' => $results]);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    //lookup PrecedingYear
    public function PrecedingYear()
    {
        try {
            $queryResult = DB::select('EXEC LookupPrecedingYear');
            $results = collect($queryResult);
            return response()->json(['user' => $results]);
        } catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
        }
    }
    //lookup for court
    //  public function LookupCourt()
    //  {
    //      try {
    //          $queryResult = DB::select('EXEC LookupCourt');
    //          $results = collect($queryResult);
    //          return response()->json(['user' => $results]);
    //      } catch (\Exception $e) {
    //          Log::info("Excession Occured" . $e->getMessage());
    //      }
    //  }
}
