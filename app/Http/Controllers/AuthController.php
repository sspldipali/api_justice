<?php

namespace App\Http\Controllers;

use App\User;
use Config;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use JWTAuth;
use Keygen;
use PHPMailer\PHPMailer\PHPMailer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\DbLog;
use Exception;

class AuthController extends Controller
{
    public $loginAfterSignUp = true;

    /**
     * This method used to register user by admin and password is send to user on register email ID
     */
    public function register(Request $request)
    {
        try {
            Log::info("In registerAPI method");
            \LogActivity::addToLog('User added successfully.');
            $validator = Validator::make($request->all(), [
                'Email' =>  'required|email|unique:users',
                'Name' => 'required'
            ]);

            if ($validator->fails()) {
                Log::info("Validations==>" . $validator->messages());
                $errorMsgArr = array();
                Log::info("type: " . gettype($validator->messages()));
                $msgs = json_decode(json_encode($validator->messages()));
                Log::info("msg: " . json_encode($msgs));
                foreach ($msgs as $key => $msgArr) {
                    Log::info("key==>" . $key);
                    Log::info("msgArr==>" . json_encode($msgArr));
                    array_push($errorMsgArr, $msgArr[0]);
                }
                return response()->json(array('message' => implode(",", $errorMsgArr)), 400);
            } else {

                $user = new User();
                $user->Name = $request->Name;
                $user->Email = $request->Email;
                $user->Role = $request->Role;
                $user->Court = $request->Court;
                $user->Notification = $request->Notification;
                $user->Active = $request->Active;
                $length = (int) $request->input('length', 8);
                $salt = Keygen::alphanum($length)->generate();
                $user->PasswordSalt = $salt;
                $user->password = bcrypt($salt);
                $user->save();
                Log::info("RequestData====>" . json_encode($request->all()));
                $loginModel = new User();
				//Log::info("USer===>".json_encode($user));
                if ($user != null) {

				   $access_url = env("FRONTEND_URL");
					$to = $user->Email;
					$subject = "ผลการลงทะเบียนผู้ใช้ สำนักงานศาลยุติธรรม";
                   /* $emailBody =
							"ยินดีต้อนรับ คุณ " . $user->Name . ",<br>" .
							"<p>เจ้าหน้าที่สำนังานศาลยุติธรรมได้ลงทะเบียนผู้ใช้ให้กับท่านเรียบร้อยแล้ว</p>" .
							"<p>" .
							"ชื่อผู้ใช้ของท่านคือ :" . $user->Name . "<br>" .
							"รหัสผ่านของท่านคือ :" . $user->PasswordSalt . "<br><br>".
							"ท่านสามารถเข้าใช้งานระบบได้ที่ :" . $access_url . "<br>";*/
					$emailBody =
					"<p>ยินดีต้อนรับ คุณ " . $user->Name . "</p>".
					"<p>เจ้าหน้าที่สำนักงานศาลยุติธรรมได้ลงทะเบียนผู้ใช้ให้กับท่านเรียบร้อยแล้ว<br /> ในเว็บไซต์ระบบฐานข้อมูลการสืบหาหลักทรัพย์และการผ่อนชำระค่าปรับกรณีที่ผิดสัญญาประกันในดคดีอาญา</p>".
					"<p>ชื่อผู้ใช้ของท่านคือ : " . $user->Name . "<br />".
					"รหัสผ่านของท่านคือ : " . $user->PasswordSalt . "</p>".
					"<p>ท่านสามารถเข้าใช้งานระบบได้ที่<br /> <a href=\"" . $access_url . "\" target=\"_blank\">" . $access_url . "</a></p>".
					"<p>สำนักงานศาลยุติธรรม</p>";

                    $isEmailSent = sendEmail($to, $subject, $emailBody);
                    if ($isEmailSent) {
                        $user->Notification = 1;
                        $user->save();
                    }

                    if ($user) {
                        $users = DB::table('users')
                            ->select('users.*')
                            ->get();
                        return response(array(
                            'error' => false,
                            'message' => 'User inserted successfully',
                            'user' => $users,

                        ), 200);
                    } else {
                        return response()->json([
                            'success' => false,
                        ], 404);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
            Log::info("Exception===>" . $e->getMessage());
            throw new \App\Exceptions\LogData($e);
        }
    }
    /**
     * This method used to update password by admin and updated password
     * will sent to user admin
     */

    public function update_password_by_admin(Request $request, $id)
    {

        try {
            \LogActivity::addToLog('User password reset successfully by admin.');
            $length = (int) $request->input('length', 8);
            $salt = Keygen::alphanum($length)->generate();
            $user = DB::table('users')
                ->where('id', $id)
                ->update(['password' => bcrypt($salt), 'PasswordSalt' => $salt]);
            $loginModel = new User();
            if ($user) {
                $users = DB::table('users')
                    ->select('users.*')
                    ->get();
                $mail = new PHPMailer();

                $emailBody = "Hello" .

                    "<p>" .
                    "Your password:" . $salt . "<br>";

				$to = $user->Email;
               
				$subject = "Password changed";
                
                //$isSent = $mail->send();
				$isEmailSent = sendEmail($to, $subject, $emailBody);
                return response(array(
                    'error' => false,
                    'message' => 'User password by admin updated successfully',
                    'user' => $users,
                    'salt' => $salt,

                ), 200);
            } else {
                return response(array(
                    'error' => true,
                    'message' => 'User id not found',
                    'user' => $affected,

                ), 404);
            }
        } catch (\Exception $e) {
            Log::info("Exception: " . $e->getMessage());
            return response(array(
                "success" => false,
                "msg" => $e->getMessage(),
            ), 400);
        }
    }

    public function adminPassword(Request $request, $id)
    {

        try {
            \LogActivity::addToLog('User added successfully.');
            $validator = Validator::make($request->all(), [
                'new_password'     => 'required|min:6',
                'confirm_password' => 'required|same:new_password',

            ]);
            if ($validator->fails()) {
                Log::info("Validations==>" . $validator->messages());
                $errorMsgArr = array();
                Log::info("type: " . gettype($validator->messages()));
                $msgs = json_decode(json_encode($validator->messages()));
                Log::info("msg: " . json_encode($msgs));
                foreach ($msgs as $key => $msgArr) {
                    Log::info("key==>" . $key);
                    Log::info("msgArr==>" . json_encode($msgArr));
                    array_push($errorMsgArr, $msgArr[0]);
                }
                return response()->json(array('message' => implode(",", $errorMsgArr)), 400);
            } else {

                \LogActivity::addToLog('User password reset successfully by admin.');
                $user = DB::table('users')
                    ->where('id', $id)
                    ->update(['password' => bcrypt($request->new_password)]);
                $loginModel = new User();
                if ($user) {
                    $users = DB::table('users')
                        ->select('users.*')
                        ->get();
                    $mail = new PHPMailer();

                    $emailBody = "Hello" .

                        "<p>" .
                        "Your password:" . $request->new_password . "<br>";
                				
                    $subject = "Password changed";
					$to = 'beds3@coj.go.th';
                    //$mail->SMTPDebug  = 3;
                   
                    //$isSent = $mail->send();
					$isEmailSent = sendEmail($to, $subject, $emailBody);
                    return response(array(
                        'error' => false,
                        'message' => 'User password by admin updated successfully',
                        'user' => $users,

                    ), 200);
                } else {
                    return response(array(
                        'error' => true,
                        'message' => 'User id not found',

                    ), 404);
                }
            }
        } catch (Exception $e) {
            Log::info("Something went wrong while fetching company list! Exception Message: " . $e->getMessage());
            Log::error($e);
        }
    }
    /**
     * This method used ti login to the app it will generate jwt token for authentication
     */
    public function login(Request $request)
    {
        try {

            $input = $request->only('Name', 'password');

            $users_info = DB::table('users')
                ->where('Role', 4)
                ->get();

            if ((!$jwt_token = JWTAuth::attempt($input))) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Name or Password',
                ], 401);
            }
            session(['user_id' => $request->user()->id]);
            \LogActivity::addToLog('login successfully.');
            return response()->json([
                'success' => true,
                'token' => $jwt_token,
                "id" => $request->user()->id,
                "Name" => $request->user()->Name,
                "Email" => $request->user()->Email,
                "Role" => $request->user()->Role,
				"Court" => $request->user()->Court

            ]);
        } catch (\Exception $e) {
            throw new \App\Exceptions\LogData($e);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function logActivity()
    {
        $logs = \LogActivity::logActivityLists();
        return view('logActivity', compact('logs'));
    }
    /**
     * This method used to logout from application
     */
    public function logout(Request $request)
    {

        $this->validate($request, [
            'token' => 'required',
        ]);

        try {

            JWTAuth::invalidate($request->token);
            Session::flush();
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully',
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out',
            ], 500);
        }
    }

    /**
     * This method used to get all users from user table and its role from roles table
     */
    
    public function getAuthUser()
    {
        try {
            $Authuser = JWTAuth::parseToken()->authenticate();
            if ($Authuser) {
                if ($Authuser->Role == 2) {
                    $users = DB::table('users')
                        ->join('Role', 'users.Role', '=', 'Role.Id')
                        ->leftJoin('VCourt AS c', 'users.Court', '=', 'c.id')
                        ->select('users.*', 'Role.RoleName', 'c.Name AS CourtName')

                        ->get();
                    return response()->json(['user' => $users]);
                } else if ($Authuser->Role == 1 || $Authuser->Role == 3) {
                    $users = DB::table('users')
                        ->join('Role', 'users.Role', '=', 'Role.id')
                        ->leftJoin('VCourt AS c', 'users.Court', '=', 'c.id')
                        ->select('users.*', 'Role.RoleName', 'c.Name AS CourtName')
                        ->where('users.id', $Authuser->id)
                        ->get();
                    return response()->json(['user' => $users]);
                }
            }
        } catch (Exception $e) {
            Log::info("Excession Occured".$e->getMessage());
        }
    }
	
			 
    /**
     * This method used to get users roles
     */
    public function getUserRoles()
    {
        try {
            $users = DB::table('Role')
                ->select('Role.*')
                ->get();
            return response()->json(['user' => $users]);
        } catch (\Exception $e) {
            Log::info("Excession Occured", $e->getMessage());
        }
    }
    /**
     * This method used to get court  look up data
     */
    public function LookupCourt()
    {
        try {
            $users = DB::table('VCourt')
                ->select('VCourt.*')
                ->get();
            return response()->json(['user' => $users]);
        } catch (\Exception $e) {
            Log::info("Excession Occured". $e->getMessage());
        }
    }
    /**
     * This method used to delete user record from user table by id
     */
    public function delete($id)
    {
        \LogActivity::addToLog('User deleted successfully.');
        try {
            $affected = User::find($id)->delete();
            if ($affected) {
                $users = DB::table('users')

                    ->select('users.*')
                    ->get();
                return response(array(
                    'error' => false,
                    'message' => 'User deleted successfully',
                    'user' => $users,

                ), 200);
            } else {
                return response(array(
                    'error' => true,
                    'message' => 'User id not found',
                    'user' => $affected,

                ), 404);
            }
        } catch (\Exception $e) {
            Log::info("Excession Occured".$e->getMessage());
        }
    }
    /**
     * This method used to update user record by id in users table
     */
    public function update(Request $request, $id)
    {

        try {
            $validator = Validator::make($request->all(), [
                'Name' => 'required',

            ]);
            if ($validator->fails()) {
                Log::info("Validations==>" . $validator->messages());
                $errorMsgArr = array();
                Log::info("type: " . gettype($validator->messages()));
                $msgs = json_decode(json_encode($validator->messages()));
                Log::info("msg: " . json_encode($msgs));
                foreach ($msgs as $key => $msgArr) {
                    Log::info("key==>" . $key);
                    Log::info("msgArr==>" . json_encode($msgArr));
                    array_push($errorMsgArr, $msgArr[0]);
                }
                return response()->json(array('message' => implode(",", $errorMsgArr)), 400);
            } else {

                $affected = DB::table('users')
                    ->where('id', $id)
                    ->update([
                        'Name' => $request->input('Name'), 'Email' => $request->input('Email'),
                        'Role' => $request->input('Role'), 'Court' => $request->input('Court'),
                        'Notification' => $request->input('Notification'), 'Active' => $request->input('Active')
                    ]);

                if ($affected) {
                    return response(array(
                        'error' => false,
                        'message' => 'User updated successfully'

                    ), 200);
                } else {
                    return response(array(
                        'error' => true,
                        'message' => 'User id not found'
                        // 'user'=> $affected

                    ), 404);
                }
            }
        } catch (Exception $e) {
            Log::info("Something went wrong while Updating user record! Exception Message: " . $e->getMessage());
            Log::error($e);
            return response(array(
                'error' => true,
                'message' => 'Something went wrong while updating user record',
                'exception' => $e->getMessage()
            ), 404);
        }
    }
    /**
     * This method used to get user by id for manage user screen
     */
    public function get_user_by_id($id)
    {
        $user = DB::table('users')
            ->select('users.*')
            ->where([
                ['id', $id],

            ])
            ->get();
        return response()->json(['user' => $user]);
    }
    /**
     * This method used to send password reset link on registerd email ID
     */
    public function sendResetLink(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email',

            ]);

            if ($validator->fails()) {
                Log::info("Validations==>" . $validator->messages());
                $errorMsgArr = array();
                Log::info("type: " . gettype($validator->messages()));
                $msgs = json_decode(json_encode($validator->messages()));
                Log::info("msg: " . json_encode($msgs));
                foreach ($msgs as $key => $msgArr) {
                    Log::info("key==>" . $key);
                    Log::info("msgArr==>" . json_encode($msgArr));
                    array_push($errorMsgArr, $msgArr[0]);
                }

                return response()->json(array('message' => implode(",", "$errorMsgArr")), 400);
            } else {
                $url = Config::get('url.url');

                $EmailID = $request->input("email");
                $loginModel = new User();
                $user = $loginModel->getUserByEmailId($EmailID);
                $userID = $user->id;

                $uniqueURLIdentifier = Carbon::now('UTC')->addMinutes(15)->timestamp . "" . $user->id;

                $resetURL = $url . "/" . $uniqueURLIdentifier;

                if ($user != null) {
                               
					$emailBody = 
					"<p>เรียน คุณ " . $user->Name . "</p>".
					"<p>ท่านแจ้งลืมรหัสผ่านในเว็บไซต์ระบบฐานข้อมูลการสืบหาหลักทรัพย์และการผ่อนชำระค่าปรับกรณีที่ผิดสัญญาประกันในดคดีอาญา <br />ขอให้เข้าไปยังลิงค์</p>".
					"<p><a href=\"" . $resetURL . "\" target=\"_blank\">" . $resetURL . "</a></p>".
					"<p>เพื่อยกเลิกรหัสผ่านเดิมและขอรับรหัสผ่านใหม่</p>".
					"<p>หากท่านไม่ได้เป็นผู้แจ้งลืมรหัสผ่าน โปรดเพิกเฉยต่ออีเมลฉบับนี้<br />สำนักงานศาลยุติธรรม</p>";

					$subject = "ผลการแจ้งลืมรหัสผ่าน";
                   
					$to = $request->input("email");
                    Log::info("EMail==>".$to);
					
					$isEmailSent = sendEmail($to, $subject, $emailBody);
					Log::info("isEmailSent===>".$isEmailSent);
                    if ($isEmailSent) {
                        \LogActivity::addToLog('Password reset link sent successfully.');
                        return response()->json([
                            'success' => true,
                            'Message' => 'sent reset link email',

                        ], 200);
                    } else {
                        return response()->json([
                            'success' => false,
                            'Message' => 'reset link email failed',

                        ], 400);
                    }
                }
            }
        } catch (Exception $e) {
            Log::info("Something went wrong while fetch user record! Exception Message: " . $e->getMessage());
            Log::error($e);
            return response()->json([
                'success' => false,
                'Message' => 'Something went wrong',
                'exception' => $e->getMessage()
            ], 400);
        }
    }
    /**
     * This method used to reset password by id
     */
    public function resetForgotPassword(Request $request, $uniqueURLIdentifier)
    {
        try {

            $id = substr($uniqueURLIdentifier, 10);
			Log::info("Id===>".$id);
            $expiryTime = substr($uniqueURLIdentifier, 0, 10);

            $now = Carbon::now('UTC');
            $minutes = $now->diffInMinutes(new Carbon((int) $expiryTime));
			if ($minutes > 0 && $minutes <= 1440) { 
					 $validator = Validator::make($request->all(), [
						'newPassword' => 'required',

					]);

					if ($validator->fails()) {
						Log::info("Validations==>" . $validator->messages());
						$errorMsgArr = array();
						Log::info("type: " . gettype($validator->messages()));
						$msgs = json_decode(json_encode($validator->messages()));
						Log::info("msg: " . json_encode($msgs));
						foreach ($msgs as $key => $msgArr) {
							Log::info("key==>" . $key);
							Log::info("msgArr==>" . json_encode($msgArr));
							array_push($errorMsgArr, $msgArr[0]);
						}

						return response()->json(array('message' => implode(",", "$errorMsgArr")), 400);
					} else {
                    //\LogActivity::addToLog('Forgot password reset successfully.');

					$newEncPass = bcrypt($request->newPassword);
					
					Log:info("NEW PWD " . $newEncPass);
					
                    $affected = DB::table('users')
                        ->where('id', $id)
                        ->update(['password' => $newEncPass]);
					Log::info("IsAffected===>".$affected);
                    if ($affected) {
                        $users = DB::table('users')
                            ->select('users.*')
                            ->get();
                        return response(array(
                            'error' => false,
                            'message' => 'Password updated successfully',
                            'user' => $users,

                        ), 200);
                    } else {
                        return response(array(
                            'error' => true,
                            'message' => 'User id not found',
                            'user' => $affected,

                        ), 404);
                    }
				} 
			} else {
                //echo "<h2>This link is expired</h2>";
                return response()->json(array('message' => "The link is expired"), 400);
            }
        } catch (Exception $e) {
            Log::info("Something went wrong hile forgot/reset password! Exception Message: " . $e->getMessage());
            Log::error($e);
        }
    }

    public function testEmail(Request $request)
    {
        Log::info("Reached: testEmail");
        $to = "devphp11@gmail.com";
        $cc = "dpatil1390@gmail.com";
        $bcc = "patildipa1390@gmail.com";
        $subject = "Test Email";
        $emailBody ="เรียน คุณ  <br>".
						"<p>ท่านสามารถเข้าดูรายงานข้อมูลการสืบหาหลักทรัพย์และการผ่อนชำระค่าปรับกรณีที่ผิดสัญญาประกันในคดีอาญา ประจำปีที่  ไตรมาสที่  ได้ที่ <br>".
						"<p style='margin-left:80%;'><b>สำนักงานศาลยุติธรรม<b></p><br>";

        $isEmailSent = sendEmail($to, $subject, $emailBody, $cc, $bcc);
        return response()->json(array("isEmailSent" => $isEmailSent), 200);
    }
}
