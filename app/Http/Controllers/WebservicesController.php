<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Zipper;
use Illuminate\Support\Facades\Log;
use Config;
class WebservicesController extends Controller
{

    /*
     *This method used to make small web serive for Vcase
     */
   
	public function getVcase(Request $request)
    {
		try{
			$Court = $request->Court;
			$finalCourt = trim($Court, '"');
			$token = $request->token;
			Log::info("CourtName==>".$Court);
			Log::info("finalCourt==>".$finalCourt);
			Log::info("Token==>".$token);
			
			$BlackNumber = $request->BlackNumber;
			$finalBlackNo = trim($BlackNumber, '"');
			Log::info("BlackNumber==>".$BlackNumber);
			Log::info("BlackNumber==>".$finalBlackNo);
			try {
				$VCase = DB::select(DB::raw("SELECT * FROM VCase WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
				return response()->json(['success' => true,'VCaseData' => $VCase], 200);
            } catch (\PDOException $pE) {
                Log::info("Excession Occured" . $pE->getMessage());
                Log::error($pE);
                return response()->json(array("message" => $pE->getMessage()), 400);
            }			
			
		}catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
            Log::error($e);
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400);
        }
		
    }
	 
    public function caseall()
    {
        $VCase = DB::table('Vsubmission')
            ->select('Vsubmission.*')
            ->get();
        return response()->json(['user' => $VCase]);

    }


    /*
     *This method used to make small web serive for Vsubmission
     */
    public function getVsubmission($Year, $Quarter)
    {
		try {
			    $Vsubmission = DB::table('Vsubmission')
					->select('Vsubmission.*')
					->where([
						['Year', $Year],
						['Quarter', $Quarter],
					])
					->get();
					
				return response()->json(['user' => $Vsubmission],200);
		}
		catch (\Exception $e) {
            Log::info("Excession Occured" . $e->getMessage());
            Log::error($e);
            return response()->json(array("message" => $e->getMessage()), 400);
        }

    }

    /*
     *This method used to make small web serive for Vinsurer
     */
    public function getVinsurer(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"'); 
               Log::info("CourtName==>".$Court); 
               Log::info("finalCourt==>".$finalCourt); 
                
                
               $BlackNumber = $request->BlackNumber; 
               $finalBlackNo = trim($BlackNumber, '"'); 
               Log::info("BlackNumber==>".$BlackNumber); 
               Log::info("BlackNumber==>".$finalBlackNo); 
               
			   $VInsurer = DB::select(DB::raw("SELECT * FROM Vinsurer WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
               
               return response()->json(['success' => true,'VInsurerData' => $VInsurer], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }
    /*
     *This method used to make small web serive for VAccused
     */

    public function getVAccused(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"'); 
               Log::info("CourtName==>".$Court); 
               Log::info("finalCourt==>".$finalCourt); 
                
                
               $BlackNumber = $request->BlackNumber; 
               $finalBlackNo = trim($BlackNumber, '"'); 
               Log::info("BlackNumber==>".$BlackNumber); 
               Log::info("BlackNumber==>".$finalBlackNo); 
               
			   $VAccused = DB::select(DB::raw("SELECT * FROM VAccused WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
               return response()->json(['success' => true,'VAccusedData' => $VAccused], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }

    /*
     *This method used to make small web serive for Vplaintiff
     */

    public function getVplaintiff(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"'); 
               Log::info("CourtName==>".$Court); 
               Log::info("finalCourt==>".$finalCourt); 
                
                
               $BlackNumber = $request->BlackNumber; 
               $finalBlackNo = trim($BlackNumber, '"'); 
               Log::info("BlackNumber==>".$BlackNumber); 
               Log::info("BlackNumber==>".$finalBlackNo); 
               
				$Vplaintiff = DB::select(DB::raw("SELECT * FROM Vplaintiff WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
			    return response()->json(['success' => true,'VplaintiffData' => $Vplaintiff], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }
    /*
     *This method used to make small web serive for VAsset
     */

    public function getVAsset(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"'); 
               Log::info("CourtName==>".$Court); 
               Log::info("finalCourt==>".$finalCourt); 
                
                
               $BlackNumber = $request->BlackNumber; 
               $finalBlackNo = trim($BlackNumber, '"'); 
				Log::info("BlackNumber==>".$BlackNumber); 
                Log::info("BlackNumber==>".$finalBlackNo); 
               
               $VAsset = DB::select(DB::raw("SELECT * FROM VAsset WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
               return response()->json(['success' => true,'VAssetData' => $VAsset], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }
    /*
     *This method used to make small web serive for VBailContract
     */
    public function getVBailContract(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"'); 
               Log::info("CourtName==>".$Court); 
               Log::info("finalCourt==>".$finalCourt); 
                
                
               $BlackNumber = $request->BlackNumber; 
               $finalBlackNo = trim($BlackNumber, '"'); 
               Log::info("BlackNumber==>".$BlackNumber); 
               Log::info("BlackNumber==>".$finalBlackNo); 
               
			    $VBailContract = DB::select(DB::raw("SELECT * FROM VBailContract WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
                return response()->json(['success' => true,'VBailContractData' => $VBailContract], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }
    
    /*
     *This method used to make small web serive for VFine
     */
    public function getVFine(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"'); 
               Log::info("CourtName==>".$Court); 
               Log::info("finalCourt==>".$finalCourt); 
                
                
               $BlackNumber = $request->BlackNumber; 
               $finalBlackNo = trim($BlackNumber, '"'); 
               Log::info("BlackNumber==>".$BlackNumber); 
               Log::info("BlackNumber==>".$finalBlackNo); 
               
			   $VFine = DB::select(DB::raw("SELECT * FROM VFine WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
               return response()->json(['success' => true,'VFineData' => $VFine], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }

    /*
     *This method used to make small web serive for VST1
     */

    public function VST1(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"');
               Log::info("CourtName==>".$Court);
               Log::info("finalCourt==>".$finalCourt);
                
                
               $BlackNumber = $request->BlackNumber;
               $finalBlackNo = trim($BlackNumber, '"');
               Log::info("BlackNumber==>".$BlackNumber);
               Log::info("BlackNumber==>".$finalBlackNo); 
                               
               
			   $VST1 = DB::select(DB::raw("SELECT * FROM VST1 WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
               return response()->json(['success' => true,'VST1Data' => $VST1], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }
    /*
     *This method used to make small web serive for VST2
     */

    public function VST2(Request $request) 
    { 
          try{ 
               $Court = $request->Court; 
               $finalCourt = trim($Court, '"'); 
               Log::info("CourtName==>".$Court); 
               Log::info("finalCourt==>".$finalCourt); 
                
                
               $BlackNumber = $request->BlackNumber; 
               $finalBlackNo = trim($BlackNumber, '"'); 
               Log::info("BlackNumber==>".$BlackNumber); 
               Log::info("BlackNumber==>".$finalBlackNo); 
                
			   $VST2 = DB::select(DB::raw("SELECT * FROM VST2 WHERE Court = N'". $finalCourt . "' AND BlackNumber = N'" . $finalBlackNo ."'")); 
               return response()->json(['success' => true,'VST2Data' => $VST2], 200); 
          }catch (\Exception $e) { 
            Log::info("Excession Occured" . $e->getMessage()); 
            Log::error($e); 
            return response()->json(array("message" => $e->getMessage(),'success' => false), 400); 
        } 
           
    }
    /*
     *This method used to make small web serive for VSTattachmet
     *get all pdf from uplaod folder and filter by year and quarter and zip it and downloaded.
     */
    public function vattachment(Request $request, $quarter, $year)
    {
        try {

            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );
			//$folderPath = public_path() . DIRECTORY_SEPARATOR . "PDF_Files";
			
			//$folderPath = env("STATTACHMENT_PATH");
			$folderPath = Config::get('url.STATTACHMENT_PATH');
			//$compareYr = $year + 543;
			$fileName = $quarter . $year;
			$files = glob($folderPath . DIRECTORY_SEPARATOR . "*" . $fileName . "*");
			$totalFiles = sizeof($files);
			$zipFilePath = public_path($fileName . ".zip");
			$zipper = \Zipper::make($zipFilePath);
			
			if(sizeof($files) > 0) {
				foreach ($files as $file) {
					$zipper->add($file);
				}
				$zipper->close();

				return response()->download($zipFilePath);
			} else {
				return response(array("data" => array(),
                "message" => "No Data Found!"),
                400,
                array("Content-Type" => "application/json")
            );
			}

        } catch (Exception $e) {
            Log::info("Exception occured while generate zip! Exception Message: " . $e->getMessage());
            return response(array("data" => array(),
                "message" => "Something went wrong!"),
                400,
                array("Content-Type" => "application/json")
            );
        }

    }
	
	/**This method is used to get encoded values */
	public function getEncodedValues(Request $request) {
		try{
			$requestBody = $request->all();
			
			$Court = $requestBody['Court'];
			$BlackNumber = $requestBody['BlackNumber'];
			
			$endodedCourt = base64_encode($Court);
			Log::info("EncodedCourtAPI" .$endodedCourt);
			
			$encodedBlackNumber = base64_encode($BlackNumber);
			Log::info("encodedBlackNo===>".$encodedBlackNumber);
			
			return response()->json(array('CourtName' => $endodedCourt, 'BlackNo' => $encodedBlackNumber));
		} catch (Exception $e) {
            Log::info("Exception occured while encoding! Exception Message: " . $e->getMessage());
            return response(array("data" => array(),
                "message" => "Something went wrong!"),
                400,
                array("Content-Type" => "application/json")
            );
        }

		
	}
}
