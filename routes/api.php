<?php

use App\User;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/send/email/submission/{finYear}/{quarter}", "SubmissionController@sendBackgroundEmailsForSubmissionNotification");
Route::get("/streceive/check", "SubmissionController@checkSTReceive");
    //login
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => 'auth.jwt'], function () {

		//GetSubmissionDetaisl API
		Route::get('/submission/details/{submissionID}', 'SubmissionController@getSubmissionDetailsByID');
		
        Route::post('/STSend', 'SubmissionController@saveSubmission');
        // All charts
        // Api for pie charts including court
        Route::get('pie_in_api', 'ChartController@chart1');
        Route::get('pie_in_api_2', 'ChartController@chart1_2');
        Route::get('pie_in_api_3', 'ChartController@chart1_3');
        Route::get('pie_in_api_4', 'ChartController@chart1_4');
        // Api for pie charts within court
        Route::get('pie_wi_api/{Juridiction}', 'ChartController@chart2_1');
        Route::get('pie_wi_api_2/{Juridiction}', 'ChartController@chart2_2');
        Route::get('pie_wi_api_3/{Juridiction}', 'ChartController@chart2_3');
        Route::get('pie_wi_api_4/{Juridiction}', 'ChartController@chart2_4');

        // // Api for line charts including court
        Route::get('line_in_api_5/{FiscalYear}', 'ChartController@chart1_5');
        Route::get('line_in_api_6/{FiscalYear}', 'ChartController@chart1_6');
        Route::get('line_in_api_7/{FiscalYear}', 'ChartController@chart1_7');
        Route::get('line_in_api_8/{FiscalYear}', 'ChartController@chart1_8');
        // Api for line charts within court
        Route::get('line_wi_api_5/{FiscalYear}/{Jurisdiction}', 'ChartController@chart2_5');
        Route::get('line_wi_api_6/{FiscalYear}/{Jurisdiction}', 'ChartController@chart2_6');
        Route::get('line_wi_api_7/{FiscalYear}/{Jurisdiction}', 'ChartController@chart2_7');
        Route::get('line_wi_api_8/{FiscalYear}/{Jurisdiction}', 'ChartController@chart2_8');

        // Api for bar charts including court
        Route::get('bar_in_api_9/{PrecedingQtr}', 'ChartController@chart1_9');
        Route::get('bar_in_api_10/{PrecedingQtr}', 'ChartController@chart1_10');
        Route::get('bar_in_api_11/{PrecedingQtr}', 'ChartController@chart1_11');
        Route::get('bar_in_api_12/{PrecedingQtr}', 'ChartController@chart1_12');
        Route::get('bar_in_api_13/{PrecedingYear}', 'ChartController@chart1_13');
        Route::get('bar_in_api_14/{PrecedingYear}', 'ChartController@chart1_14');
        Route::get('bar_in_api_15/{PrecedingYear}', 'ChartController@chart1_15');
        Route::get('bar_in_api_16/{PrecedingYear}', 'ChartController@chart1_16');

        // Api for bar charts within court
        Route::get('bar_wi_api_9/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_9');
        Route::get('bar_wi_api_10/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_10');
        Route::get('bar_wi_api_11/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_11');
        Route::get('bar_wi_api_12/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_12');
        Route::get('bar_wi_api_13/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_13');
        Route::get('bar_wi_api_14/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_14');
        Route::get('bar_wi_api_15/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_15');
        Route::get('bar_wi_api_16/{Jurisdiction}/{PrecedingQtr}', 'ChartController@chart2_16');


        Route::group(['middleware' => ['auth.jwt', 'otheruser']], function () {
        });
        //access report

        Route::get('get_access_log_data', 'AuditController@get_access_log_data');
        Route::get('getAuditLog', 'AuditController@getAuditLog');

        //error log

        Route::post('get_error_log', 'AuditController@getErrorLog');


        //get_User

        //get users roles
        
        Route::get('get_User', 'AuthController@getAuthUser');
        //get passowrd
        Route::get('/password', 'AuthController@showRandomPassword');
        // //delete user
        // Route::delete('user/{id}', 'AuthController@delete');

        Route::post('/report/preview', 'SubmissionController@getReportPreview');
        Route::post('/report/history', 'SubmissionController@getReportHistory');

        //Submission Screen
        //Route::post('/STSend', 'SubmissionController@saveSubmission');
    });
    Route::post('sendResetLink', 'AuthController@sendResetLink');
    Route::post('resetForgotPassword/{uniqueURLIdentifier}', 'AuthController@resetForgotPassword');
	
    //Route::post('/STSend', 'SubmissionController@saveSubmission');
    //delete access report

    Route::post('delete_access_report/{id}', 'AuditController@delete_access_report');

    //delete error log
    Route::post('delete_error_log/{id}', 'AuditController@delete_error_log');

    //update password by admin

    Route::post('adminPassword/{id}', 'AuthController@adminPassword');  //This method is used to set new passwordbyadmin
    Route::post('update_password_by_admin/{id}', 'AuthController@update_password_by_admin');

    //update user by id

    //get user by id
    Route::get('get_user_by_id/{id}', 'AuthController@get_user_by_id');
    Route::post('register', 'AuthController@register');
    //delete user

    Route::post('user/{id}', 'AuthController@delete');

   //small webservices
    Route::group(['middleware' => ['auth.jwt', 'apiuser']], function () {
    Route::get('Cases', 'WebservicesController@getVcase');
    Route::get('caseall', 'WebservicesController@caseall');
	  Route::get('Plaintiffs', 'WebservicesController@getVplaintiff');
    Route::get('Defendants', 'WebservicesController@getVAccused');
    Route::get('Insurers', 'WebservicesController@getVinsurer');
    Route::get('Assets', 'WebservicesController@getVAsset');
    Route::get('InsuranceContracts', 'WebservicesController@getVBailContract');
    Route::get('Fines', 'WebservicesController@getVFine');
    Route::get('ST13', 'WebservicesController@VST1');
    Route::get('ST24', 'WebservicesController@VST2');
	  Route::get('STSummaries/{Year}/{Quarter}', 'WebservicesController@getVsubmission');

    //attachment
    Route::get('STAttachments/{year}/{quarter}', 'WebservicesController@vattachment');
});
Route::group(['middleware' => 'cors'], function () {
    Route::post('sendResetLink', 'AuthController@sendResetLink');
    Route::post('resetForgotPassword/{id}', 'AuthController@resetForgotPassword');
    Route::get('getUserRoles', 'AuthController@getUserRoles');
    Route::post('update_user/{id}', 'AuthController@update');
    Route::get('LookupCourt', 'AuthController@LookupCourt');
    //get lookups
    Route::get('pie1', 'ChartController@FiscalYear');
    Route::get('pie2', 'ChartController@Juristiction');
    Route::get('pie3', 'ChartController@PrecedingQtr');
    Route::get('pie4', 'ChartController@PrecedingYear');
    // Route::get('LookupCourt', 'ChartController@LookupCourt');
});
//attachment
Route::get('file_zip/{year}/{quarter}', 'WebservicesController@file_zip');
Route::get('downloadZip', 'WebservicesController@downloadZip');
Route::get('logout', 'AuthController@logout');


Route::get('/getfilepath/{quarter}/{year}', 'SubmissionController@getReceiptFilePath');
Route::get('/getyrqtr', 'SubmissionController@getFiscalYearQtr');
//Route::post('encoded/values', 'WebservicesController@getEncodedValues');
