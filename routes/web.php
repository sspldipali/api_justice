<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('add-to-log', 'AuditController@myTestAddToLog');
Route::get('logActivity', 'AuditController@logActivity');
Route::get("/fyear", "SubmissionController@getFYear");
Route::get("/test/email", "AuthController@testEmail");
Route::get("/test/obj", "SubmissionController@testObj");